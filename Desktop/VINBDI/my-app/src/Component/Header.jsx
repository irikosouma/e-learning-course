import React, { Component } from 'react'
import '../_core/headerStyle.scss';
export default class Header extends Component {
  render() {
    return (
        <div id="top-header" className="py-3">
            <div className="container-fluid">
                <div className="row">
                    <div className="col-md-7 text-center text-md-left header-left pt-1">
                        <i className="fa fa-phone"></i>
                        <span className="mr-3">1234567889</span>                                                     
                        <i className="fa fa-envelope ml-4"></i>
                        <span className="text text-white">VINDBI- Bring new experience for customer</span>
                    </div> 
                    <div className="col-md-5 text-center text-md-center header-right text-right  mt-2 mt-md-0">
                        <i className="fab fa-facebook-f"></i>
                        <i className="fab fa-twitter-square"></i>
                        <i className="fab fa-twitch"></i>
                        <i className="fab fa-linkedin"></i>
                    </div>                        
                </div>        
            </div>          
        </div>
    )
}
}

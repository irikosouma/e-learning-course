import React from 'react';

import './App.css';

import AppRoot from "./screen/AppRoot.jsx";


class App extends React.Component {
  render() {
    return (
      
      <AppRoot/>
    )  
  }

}

export default App;
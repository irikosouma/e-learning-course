import React from "react";
import { Wrapper } from "./MainList.styles";
import Manage from "./Manage";

export default function MainList() {
  return (
    <Wrapper>
      <Manage />
    </Wrapper>
  );
}

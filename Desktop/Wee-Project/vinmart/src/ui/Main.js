import React from "react";
import { WrapperContent, Header } from "./Main.styles";
import MainList from "./MainList";
import Menu from "./Menu";

export default function Main() {
  return (
    <>
      <Header>
        <h1 className="logo">Logo</h1>
      </Header>
      <WrapperContent>
        <Menu />
        <MainList />
      </WrapperContent>
    </>
  );
}

import React, { useState } from "react";
import styled from "styled-components";
import { dataUser } from "../data/dataUser";
const Wrapper = styled.div`
  width: 100%;
  height: 100%;
  font-family: "Roboto-Bold";
  background-color: #fafafa;
  & > .box-name {
    /* font-family: "Lato-Bold"; */
    font-size: 20px;
    width: calc(100% - 88px);
    height: 32px;
    border-bottom: 1px solid #e0e0e0;
    margin: 53px 0px 46px 44px;
    padding-bottom: 2px;
    display: flex;
    flex-direction: row;
    justify-content: flex-start;
    & > .box-cat-title {
      width: fit-content;
      height: 24px;
      text-transform: uppercase;
      margin-right: 80px;
      & > .box-cat-link {
        width: fit-content;
        text-decoration: none;
        border-bottom: 5px solid #ed1c24;
        padding-bottom: 6px;
        position: relative;
        padding-right: 0px;
        color: #ed1c24;
        left: 0px;
        top: 0;
      }
      & > .box-cat-right {
        width: fit-content;
        text-decoration: none;
        position: relative;
        margin-left: 15px;
        top: 0px;
        color: #595959;
      }
    }
  }
  & > .box-list {
    display: flex;
    width: calc(100% - 44px);
    justify-content: space-between;
    text-transform: uppercase;
    & > .list-none {
      display: flex;
      /* justify-content: center; */
      align-items: center;
      color: #595959;
      width: 413px;
      height: 37px;
      padding-right: 10.3%;
      padding-left: 44px;
      font-size: 15px;
    }
    & > .list-color {
      display: flex;
      justify-content: center;
      align-items: center;
      background-color: #ffc107;
      width: 499px;
      height: 40px;
      border-radius: 3px;
      color: #fff;
      font-size: 14px;
      position: relative;
      i {
        position: absolute;
        border: solid white;
        border-width: 0 2px 2px 0;
        display: inline-block;
        padding: 3px;
        right: 15px;
      }
      .down {
        transform: rotate(45deg);
        -webkit-transform: rotate(45deg);
      }
    }
  }
  & > .list {
    width: calc(100% - 132px);
    height: 1fr;
    margin: 35px 0px 0 44px;
    /* margin-left: 44px; */

    display: grid;
    grid-template-columns: repeat(4, 1fr);
    grid-template-rows: 1fr;
    grid-column-gap: 28px;
    grid-row-gap: 20px;

    & > .user-list {
      width: 100%;

      height: 100%;
      display: flex;
      flex-direction: column;
      align-items: center;
      justify-content: center;
      border-radius: 10px;
      background-color: #fff;
      & > .image {
        width: 100%;
        height: 100%;
        margin-top: 14px;
        margin-left: 55%;
      }
      & > .name {
        font-size: 20px;
        font-family: "Roboto-Bold";
        line-height: 30px;
      }
      & > .phone {
        color: red;
        font-family: "Roboto-Regular";
      }
      & > .date {
        padding: 28px 0 21px 0;
        & > .gray {
          color: #595959;
        }
      }
    }
  }
`;

export default function Manage() {
  const dataList = dataUser;
  const [mainTitle, setMainTitle] = useState(0);
  const onChangeMenu = (index) => {
    setMainTitle(index);
  };
  return (
    <>
      <Wrapper>
        <div className="box-name">
          <div className="box-cat-title">
            <a
              className={mainTitle === 0 ? "box-cat-link" : "box-cat-right"}
              onClick={() => onChangeMenu(0)}
            >
              Quản trị tủ
            </a>
            {/* <a> Thao tác tủ</a> */}
          </div>
          <div className="box-cat-title">
            <a
              className={mainTitle === 1 ? "box-cat-link" : "box-cat-right"}
              onClick={() => onChangeMenu(1)}
            >
              Lịch sử thao tác tủ
            </a>
          </div>
        </div>
        <div className="box-list">
          <div className="list-none"> danh sách các admin quản lý tủ</div>
          <div className="list-color">
            Vinmart Timecity <i className="down"></i>
          </div>
        </div>
        <div className="list">
          {dataList.map((item, index) => (
            <div key={index} className="user-list">
              <div className="image">
                <img
                  src={item.UserImage}
                  style={{
                    width: 126,
                    height: 126,
                    borderRadius: "50%",
                    paddingBottom: 12,
                  }}
                />
              </div>
              <div className="name">{item.UserName}</div>
              <div className="phone">{item.Phone}</div>
              <div className="date">
                <span className="gray"> Ngày Đăng Ký: </span>
                {item.Date}
              </div>
            </div>
          ))}
        </div>
      </Wrapper>
    </>
  );
}

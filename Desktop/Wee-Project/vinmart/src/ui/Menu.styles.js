import styled from "styled-components";
const Wrapper = styled.div`
  display: grid;
  width: 100%;
  height: 100%;
  font-family: "Roboto-Light";
  grid-template-rows: 100px 125px 120px 120px 60px 1fr;

  background-color: #fff;
  & > .btn {
    height: 50px;

    appearance: none;
    outline: none;
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: space-between;
    margin: 54px 19px 47px 28px;

    & p {
      text-transform: uppercase;
    }
    & span {
      width: 20px;
      height: 20px;
      background-color: yellow;
    }

    i {
      border: solid black;
      border-width: 0 3px 3px 0;
      display: inline-block;
      padding: 3px;
      margin-right: 15px;
    }
    .up {
      transform: rotate(-135deg);
      -webkit-transform: rotate(-135deg);
    }

    .down {
      transform: rotate(45deg);
      -webkit-transform: rotate(45deg);
    }
  }
`;

export { Wrapper };

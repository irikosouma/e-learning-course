import styled from "styled-components";
const WrapperContent = styled.div`
  width: 100%;
  height: 100%;
  background-color: #f1f1f1;
  display: grid;
  grid-template-columns: 290px 1fr;
`;
const Header = styled.div`
  min-width: 1440px;
  height: 80px;
  background-color: red;
  & > .logo {
    width: 197px;
    height: 65px;
    background-color: #fff;
    margin: 0 0 15px 44px;
  }
`;

export { WrapperContent, Header };

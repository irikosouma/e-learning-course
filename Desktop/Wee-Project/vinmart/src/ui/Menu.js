import React, { useState } from "react";
import { Wrapper } from "./Menu.styles";
import SubMenu from "./SubMenu";
export default function Menu() {
  const [dropDown, setDropDown] = useState(0);
  const onClickDropMenu = (index) => {
    setDropDown(index);
  };
  const [open, setOpen] = useState(1);
  const onClickOpen = (index) => {
    setOpen(index);
  };
  return (
    <Wrapper>
      <div className="btn" onClick={() => onClickOpen(0)}>
        <span></span> <p>CAMERA GIÁM SÁT</p>
        {open === 0 ? <i className="down"></i> : <i className=" up"></i>}
      </div>
      {open === 0 ? (
        <i className="down"></i> && <SubMenu style={{ display: "block" }} />
      ) : (
        <i className="up"></i>
      )}

      <div className="btn" onClick={() => onClickOpen(1)}>
        <span></span> <p>Giám sát ngăn Tủ</p>
        {open === 1 ? <i className="down"></i> : <i className=" up"></i>}
      </div>
      {open === 1 ? (
        <i className="down"></i> && <SubMenu style={{ display: "block" }} />
      ) : (
        <i className="up"></i>
      )}
    </Wrapper>
  );
}

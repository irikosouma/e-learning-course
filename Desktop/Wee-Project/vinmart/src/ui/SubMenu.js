import React from "react";
import styled from "styled-components";
const Wrapper = styled.div`
  width: 100%;
  height: 1fr;
  & > div {
    width: 100%;
    height: 60px;
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: first baseline;
    & > .icon {
      width: 20px;
      height: 20px;
      background-color: yellow;
      margin: 0 20px 0 80px;
    }
  }
`;

export default function SubMenu() {
  return (
    <Wrapper>
      <div>
        <p className="icon"></p>Hoạt động tủ
      </div>
      <div>
        <p className="icon"></p>Lịch sử khách gửi
      </div>
      <div>
        <p className="icon"></p>Nhân viên trực
      </div>
    </Wrapper>
  );
}
